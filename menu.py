#!/usr/bin/python
# -*- coding: utf -8 -*-
#créer un menu
def menu_vide():
    menu={}
    return menu

def exit(quitter):
    print ("au revoir")
    quitter = True
    return quitter 

#ajouter une option dans le menu
def ajout_option (menu,nb,label,fonction):
    menu[str(nb)] = [label, fonction]

def afficher_menu(menu):
    for key, value in menu.items():
        print (key,':',value[0])

#traiter un choix
def choisir(menu,quitter):
    quitter_ = False
    while quitter_ == False:
        key = input ("quel est votre choix? ")
        if key in menu.keys():
            if menu[key][0] == 'quitter':
                quitter_ = True
            else:
                menu[key][1]()
        else:
            print ("je n'ai pas compris")
        
    
    

