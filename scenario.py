import labo

#initialiser le laboratoire:
laboratoire = labo.labo_vide()
print (laboratoire)

#enregistrer des arrivees: 
labo.enregistrer_arrivee(laboratoire, "Aurelie", "F307")
labo.enregistrer_arrivee(laboratoire, "Marc", "F305")
labo.enregistrer_arrivee(laboratoire, "Xavier", "F305")
labo.enregistrer_arrivee(laboratoire, "Lea", "F306")
labo.enregistrer_arrivee(laboratoire, "Jules", "F308")
labo.enregistrer_arrivee(laboratoire, "Jack", "F306")

print (laboratoire)

#enregistrer un depart
labo.enregistrer_depart(laboratoire,"Marc")
print (laboratoire)

#modifier le bureau
labo.modification_bureau (laboratoire,"Aurelie","F306")
print (laboratoire)

#modifier le nom
labo.modification_nom (laboratoire, "Lea", "Leo")
print(laboratoire)

#savoir si membre
labo.est_personnel (laboratoire, "Mariam")

#quel bureau
labo.quel_bureau (laboratoire, "Leo")

#nos personnels
labo.nos_personnels(laboratoire)

#occupation des bureaux par ordre lexicographique
labo.occupation_des_bureaux_lexic (laboratoire)

#afficher l'occupation des bureaux
labo.occupation_des_bureaux (laboratoire)