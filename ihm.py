#!/usr/bin/python
# -*- coding: utf -8-*-
import labo
import menu
import json


def main():  
    #initialiser le laboratoire   
    laboratoire = labo.labo_vide()
    quitter = False
    #afficher le menu pour accéder aux différentes fonctionnalités
    while not quitter:
        print ("que voulez vous faire? \n 1- enregistrer une arrivee \n 2- enregistrer un départ \n 3- modification bureau \n 4- modifcation nom \n 5- savoir si membre \n 6- quel bureau \n 7- nos personnels")
        
        #demander un choix d'une fonctionnalité 
        choix = int(input ("quel est votre choix? "))
        
        #traiter les choix
        
        if choix == 1: #enregistrer une arrivée

            #demander le nom et le bureau
            nom = input ("nom:")
            bureau = input ("bureau:")

            #appel à la fonction enregistrer_arrivee
            try:
                labo.enregistrer_arrivee(laboratoire,nom,bureau)
            except labo.presentException:
                print ("raté")

        elif choix==2: #enregistrer un départ
            
            #demander le nom
            nom = input ("nom:")

            #appel à la fonction enregistrer_depart
            try:
                labo.enregistrer_depart(laboratoire,nom)
            except labo.PresentException:
                print ("raté")

        elif choix == 3: #modification bureau

            #demander le nom et le nouveau bureau
            nom = input ("nom:")
            nouveau_bureau = input ("nouveau bureau:")

            #appel à la fonction modification bureau
            try:
                labo.modification_bureau (laboratoire,nom,nouveau_bureau)
            except labo.PresentException:
                print ("raté")

        elif choix == 4: #modifcation nom

            #demander l'ancien et le nouveau nom
            nom = input ("nom:")
            nouveau_nom= input ("nouveau nom:")

            #appel à la fonction modification_nom
            try:
                labo.modification_nom (laboratoire, nom, nouveau_nom)
            except labo.PresentException:
                print ("rate")

        elif choix == 5: #savoir si membre

            #demander le nom
            nom = input ("nom:")

            #appel à la fonction est_personnel
            try: 
                labo.est_personnel (laboratoire, nom)
            except labo.PresentException:
                print ("rate")

        elif choix == 6: #quel bureau

            #demander le nom
            nom = input ("nom:")

            #appel à la fonction quel_bureau
            try:
                labo.quel_bureau (laboratoire, nom)
            except labo.AbsentException:
                print ("rate")

        elif choix == 7: #nos personnels
            #appel à la fonction nos_persnnels
            labo.nos_personnels(laboratoire)

        else:
            print ("j'ai pas compris")

 ######################################################################################################

def main2():
    
    #initialiser le laboratoire   
    laboratoire = labo.labo_vide() 

    #charger les informations sauvegardés dans un fichier gestion_labo.json
    labo.json_read(laboratoire, "gestion_labo.json")
    
    #importer un fichier csv
    labo.importer_csv("fichier.csv", laboratoire )

    #imprimer l'occupation des bureaux sous forme d'une page HTML
    labo.occupation_des_bureaux_html(laboratoire )

    quitter = False

    #initialiser le menu
    menu_principal = menu.menu_vide()

    #ajouter des options dans le menu
    menu.ajout_option(menu_principal,1,"enregistrer une arrivee", enregistrer_arrivee_2(laboratoire))
    menu.ajout_option(menu_principal,2,"enregistrer un depart", enregistrer_depart_2(laboratoire))
    menu.ajout_option(menu_principal,3,"modifier un bureau", modification_bureau_2(laboratoire))
    menu.ajout_option(menu_principal,4,"modifier un nom", modification_nom_2(laboratoire))
    menu.ajout_option(menu_principal,5,"est personnel?", est_personnel_2(laboratoire))
    menu.ajout_option(menu_principal,6,"quel bureau?", quel_bureau_2(laboratoire))
    menu.ajout_option(menu_principal,7,"nos personnels", nos_personnels_2(laboratoire))
    menu.ajout_option(menu_principal,8,"quitter", quitter_2(quitter))

    #afficher le menu
    menu.afficher_menu(menu_principal)
   
    #traiter un choix
    menu.choisir(menu_principal,quitter)

    #sauvegarder les informations dans le fichier gestion_labo.json
    labo.json_write(laboratoire,"gestion_labo.json")


def enregistrer_arrivee_2(laboratoire):
    def fonction_imbriquee():
        #demander le nom et le bureau
        nom = input ("nom:")
        bureau = input ("bureau:")

        #appel à la fonction enregistrer_arrivee()
        try:
            labo.enregistrer_arrivee(laboratoire,nom,bureau)
        except labo.PresentException:
            print ("raté")
    return fonction_imbriquee


def enregistrer_depart_2(laboratoire):
    def fonction_imbriquee():
        #demander le nom
        nom = input ("nom:")

        #appel à la fonction enregistrer_depart()
        try:
            labo.enregistrer_depart(laboratoire,nom)
        except labo.PresentException:
            print ("raté")
    return fonction_imbriquee

def modification_bureau_2(laboratoire):
    def fonction_imbriquee():
        #demander le nom et le nouveau bureau
        nom = input ("nom:")
        nouveau_bureau = input ("nouveau bureau:")

        #appel à la fonction modification bureau()
        try:
            labo.modification_bureau (laboratoire,nom,nouveau_bureau)
        except labo.PresentException:
            print ("raté")
    return fonction_imbriquee

def modification_nom_2(laboratoire):
    def fonction_imbriquee():
        #demander l'ancien et le nouveau nom
        nom = input ("nom:")
        nouveau_nom= input ("nouveau nom:")

        #appel à la fonction modification_nom()
        try:
            labo.modification_nom (laboratoire, nom, nouveau_nom)
        except labo.PresentException:
            print ("raté")
    return fonction_imbriquee

def est_personnel_2(laboratoire):
    def fonction_imbriquee():
        #demander le nom
        nom = input ("nom:")

        #appel à la fonction est_personnel()
        try: 
            labo.est_personnel (laboratoire, nom)
        except labo.PresentException:
            print ("rate")
    return fonction_imbriquee

def quel_bureau_2(laboratoire):
    def fonction_imbriquee():
        #demander le nom
        nom = input ("nom:")

        #appel à la fonction quel_bureau()
        try:
            labo.quel_bureau (laboratoire, nom)
        except labo.AbsentException:
            print ("rate")
    return fonction_imbriquee

def nos_personnels_2(laboratoire):
    def fonction_imbriquee():
        #appel à la fonction nos_persnnels
        labo.nos_personnels(laboratoire)
    return fonction_imbriquee

def quitter_2(quitter):
    def fonction_imbriquee(quitter):
        menu.exit(quitter)
    return fonction_imbriquee

if __name__=='__main__':
    main2()
