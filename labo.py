#!/usr/bin/python
# -*- coding: utf -8-*-
import csv
import json

class LaboException(Exception):
    pass


class AbsentException(LaboException):
    pass


class PresentException(LaboException):
    pass

#fonction pour initialiser un dictionnaire labo_vide
def labo_vide():
    laboratoire= dict()
    return laboratoire

#fonction pour enregistrer une arrivée
def enregistrer_arrivee(laboratoire, nom, bureau):
    if nom in laboratoire:
        raise PresentException(nom)
    laboratoire[nom]= bureau

#fonction pour enregistrer un départ
def enregistrer_depart(laboratoire , nom):
    if nom not in laboratoire:
        raise AbsentException(nom)
    del laboratoire[nom]

#fonction pour modifier un bureau
def modification_bureau (laboratoire,nom,nouveau_bureau):
	laboratoire[nom] = nouveau_bureau

#fonction pour modifier un nom		
def modification_nom(laboratoire,nom,nouveau_nom):
	b = laboratoire[nom]
	del laboratoire [nom]
	laboratoire[nouveau_nom] = b

#fonction qui affiche si une personne est parmi les personnels du labo		
def est_personnel (laboratoire,nom):
	if nom in laboratoire.keys():
		print (nom," est personnel")
	else:
		print (nom," est inexistant")
			
#fonction affiche le bureau q'une personne occupe
def quel_bureau (laboratoire, nom):
	print (laboratoire[nom])

#fonction qui affiche tous les personnels du laboratoire
def nos_personnels(laboratoire):
    for key, value in laboratoire.items():
        print (key, ":", value)    

#fonction qui affiche l'occupation des bureaux
def occupation_des_bureaux (laboratoire):
    #laboratoire = {'xavier': 'F305', 'marc': 'F305', 'aurelie': 'F307'}
    inverse_laboratoire = {}
    for key, values in laboratoire.items():
        inverse_laboratoire.setdefault(values, list()).append(key)
    for key in inverse_laboratoire.keys():
        print (key, ":")
        for value in inverse_laboratoire[key]:
            print ("-", value)


#fonction qui affiche l'occupation des bureaux sous forme d'une page HTML
def occupation_des_bureaux_html (laboratoire):
    inverse_laboratoire = {}
    for key, values in laboratoire.items():
        inverse_laboratoire.setdefault(values, list()).append(key)
    html = """<! DOCTYPE html>
    <head>
        <title>occupation des bureaux</title>
    </head>
    <body>"""
    for key in inverse_laboratoire.keys():
        html = html + "<ul>" + key+ ":"
        for value in inverse_laboratoire[key]:
            ul = "<li>" + str(value) + "</li>"
            html = html + ul    
        html = html + "</ul>"
    html = html + "</body> </html>"
    
    with open("index.html","w") as fw:
        fw.write(html)



#fonction qui affiche les bureaux et leurs occupants dans l’ordre lexicographique	
def occupation_des_bureaux_lexic (laboratoire):
    inverse_laboratoire = {}
    for key, values in laboratoire.items():
        inverse_laboratoire.setdefault(values, list()).append(key)
    for key in inverse_laboratoire.keys():
        print (key, ":")
        sorted_laboratoire = sorted (inverse_laboratoire[key])
        for value in sorted_laboratoire:
            print ("-", value)


#fonction pour charger les informations sauvegardés dans un fichier json
def json_read(laboratoire, nom_fichier):
    try:
        with open (nom_fichier,"r") as fr:
            laboratoire = json.load(fr)       
    except Exception as e:
        print ("exception json load : ",e)
    #else:
    #    print (laboratoire)

#fonction pour sauvegarder les informations dans un fichier json
def json_write(laboratoire,nom_fichier):
    with open(nom_fichier,"w") as fw:
        json.dump(laboratoire, fw, indent=4)


#fonction pour importer des informations à partir d'un fichier CSV
def importer_csv(fichier, laboratoire ):
    laboratoire_different =[]
    with open(fichier) as csvfile:
        reader=csv.DictReader(csvfile)
        for row in reader:
            if row["nom"] in laboratoire.keys() and row["bureau"] != laboratoire[row["nom"]] :
                laboratoire_different[row["nom"]] = row ["bureau"]
            else:
                laboratoire[row["nom"]] = row["bureau"] 
        print (laboratoire_different)         
                    


		
	
